/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;

/**
 *
 * @author Gift
 */
import java.util.Scanner;
public class Lab02 {
    static void printWelcome(){
        System.out.println("Welcome OX");
    }
    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }
    static char currentPlayer = 'X';
    static void printTurn(){
        System.out.println(currentPlayer + " Turn");
    }
    static int row, col;
    static void inputRowCol(){
        while(true){
            Scanner sc = new Scanner(System.in);
            System.out.println("Please input row, col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if(table[row-1][col-1]=='-'){
                table[row-1][col-1] = currentPlayer;
                break;
            }
        }
    }
    static void switchPlayer(){
        if(currentPlayer=='X'){
            currentPlayer = 'O';
        }
        else{
            currentPlayer = 'X';
        }
    }
    static boolean isWin(){
        if(checkRow()){
            return true;
        }
        else if(checkCol()){
            return true;
        }
        else if(checkCross1()){
            return true;
        }
        else if(checkCross2()){
            return true;
        }
        return false;
    }
    static boolean isDraw(){
        if(checkDraw()){
            return true;
        }
        return false;
    }
    static boolean checkDraw(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(table[i][j]=='-'){
                    return false;
                }
            }
        }
        return true;
    }
    static void printWin(){
        System.out.println(currentPlayer + " win!!");
    }
    static boolean checkRow(){
        for(int i=0;i<3;i++){
            if(table[row-1][i]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
    static boolean checkCol(){
        for(int i=0;i<3;i++){
            if(table[i][col-1]!=currentPlayer){
                return false;
            }
        }
        return true;
    }
    static void printDraw(){
        System.out.println(" draw!!");
    }
    static boolean checkCross1(){
        for(int i=0;i<3;i++){
            if((table[0][2]==currentPlayer) && (table[1][1]==currentPlayer) && (table[2][0]==currentPlayer)){
                return true;
            }
        }
        return false;
    }
    static boolean checkCross2(){
        for(int i=0;i<3;i++){
            if((table[i][i]!=currentPlayer)){
                return false;
            }
        }
        return true;
    }
    static void printEnd(){
        System.out.println("End game");
    }
    static char cont = 'y' ;
    static void printCont(){
        System.out.print("Continue (y/n): ");
        Scanner sc = new Scanner(System.in);
        char c = sc.next().charAt(0);  
        cont = c;
    }
    static boolean checkCont(){
        if(cont=='y'){
            return true;
            }
          return false; 
    }
    static void setTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
               table[i][j] = '-';
            }
        }
    }
       
    
    
    public static void main(String[] args) {
        printWelcome();
        while(checkCont()){
          printTable();
          printTurn();
          inputRowCol(); 
          
          if(isWin()){
              printTable();
              printWin();
              setTable();
              printCont();
          }
          if(isDraw()){
              printTable();
              printDraw();
              setTable();
              printCont();
          }
          switchPlayer();
        }
        printEnd();
    }
}
